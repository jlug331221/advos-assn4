import DistFileSharingApp.*;
import org.omg.CosNaming.*;
import org.omg.CosNaming.NamingContextPackage.*;
import org.omg.CORBA.*;
import org.omg.PortableServer.*;
import org.omg.PortableServer.POA;

import java.util.*;
import java.io.*;

public class Test {
    static PeerServerFileSharing peerServerFileSharingImpl;

    public static final String PEER_SERVER_NAME = "PeerServer_";

    /**
     * Returns the calculated average response time.
     *
     * @param long[] responseTimes  [Response times for queries and invalidation method call.]
     *
     * @return [int]
     */
    public static float calcAvgResponseTime(long[] responseTimes) {
        float total = 0;
        for(int i = 0; i < responseTimes.length; i++) {
            total += (float) responseTimes[i];
        }

        return total / responseTimes.length;
    }

    /**
     * Make 100 sequential requests to index peer0 and output the average response time
     * for each request.
     *
     * @param String args[]
     *
     * @return [void]
     */
    public static void concurrentRequestTest(String[] args) {
        // Get a handle on peer0
        try {
            // create and initialize the ORB
            ORB orb = ORB.init(args, null);

            // get the root naming context
            org.omg.CORBA.Object objRef =
                orb.resolve_initial_references("NameService");
            // Use NamingContextExt instead of NamingContext. This is
            // part of the Interoperable naming Service.
            NamingContextExt ncRef = NamingContextExtHelper.narrow(objRef);

            // resolve the Object Reference in Naming
            peerServerFileSharingImpl = PeerServerFileSharingHelper
                                        .narrow(ncRef.resolve_str(PEER_SERVER_NAME + "peer0_1050"));

            System.out.println("Obtained a handle on peer0 object: \n\n" + peerServerFileSharingImpl + "\n");

            long[] responseTimes = new long[100];
            long startTime, endTime;
            int requestNum;
            for(int i = 0; i < 100; i++) {
                startTime = System.currentTimeMillis();
                // Make a requests to the server
                peerServerFileSharingImpl.query(PEER_SERVER_NAME + "peerInteractive_1070", 10, "file4.txt");
                endTime = System.currentTimeMillis();
                responseTimes[i] = endTime - startTime;
                requestNum = i + 1;
                System.out.println("Time of request " + requestNum + ": " + responseTimes[i]);
            }
            System.out.println("The avgerage response is " + calcAvgResponseTime(responseTimes) + "ms");
        }
        catch(Exception e) {
            System.out.println("ERROR : " + e) ;
            e.printStackTrace(System.out);
        }
    }

    /**
     * Main method.
     *
     * @param String args[]
     *
     * @return [void]
     */
    public static void main(String args[]) {
        concurrentRequestTest(args);
    }
}
