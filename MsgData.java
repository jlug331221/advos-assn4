public class MsgData {
    public String upstreamPeer, messageID;

    public MsgData(String upPeer, String mID){
        this.upstreamPeer = upPeer;
        this.messageID = mID;
    }
}
