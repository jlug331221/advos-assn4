import DistFileSharingApp.*;
import org.omg.CosNaming.*;
import org.omg.CosNaming.NamingContextPackage.*;
import org.omg.CORBA.*;
import org.omg.PortableServer.*;
import org.omg.PortableServer.POA;

import java.net.Socket;

import java.util.*;
import java.io.*;
import java.util.concurrent.*;

class PeerServerFileSharingImpl extends PeerServerFileSharingPOA
{
    // Peer object that contains information about the peerID, peer neighbors and the list
    // of files local to the peer.
    static Peer peer = new Peer();

    // Associative array to keep track of messageID's and upstream peers from where the messages
    // are sent
    static List<MsgData> msgData = new ArrayList<MsgData>();

    static List<String> neighborsRecvdMsg = new ArrayList<String>();

    static PeerServerFileSharing peerServerFileSharingImpl;
    static PeerServerFileSharing peerServerFileSharingInteraciveImpl;

    static final String PEER_SERVER_NAME = "PeerServer_";

    private ORB orb;
    private String folderPath;

    public void setORB(ORB orb_val) {
        orb = orb_val;
    }

    public void setFolderPath(String folderPath_val) {
        folderPath = folderPath_val;
    }

    public String getFolderPath() {
        return folderPath;
    }

    /**
    * Invalidate fileName if it exists in the local index of files for the peer.
    *
    * @param String fileName [Name of the file to change the consistency state.]
    *
    * @return [void]
    */
    public void invalidateFile(String messageID, String fileName) {
        //System.out.println("Hello from invalidateFile method\n");
        String localMessageID = PEER_SERVER_NAME + peer.peerID + "_" + peer.portNumber;
        if(! localMessageID.equals(messageID)) {
            System.out.println("invalidateFile called -> Checking if file exists in my local directory...");
            for(MyFileObj mf: peer.localFiles) {
                if(mf.fileName.equals(fileName)) {
                    // Modify consistency state for the file that matches 'fileName'
                    mf.consistencyState = "invalid";
                    System.out.println(mf.fileName + " consistency state changed to " + mf.consistencyState +  ".\n");
                }
            }
        }
    }

    /**
    * Propogates a query like message throughout the system to all neighbors of the peer
    * and invalides all cached versions of a file.
    *
    * @param String messageID [Message ID that includes the peerID and port number.]
    * @param String originPeerID [Invalidation origin peer ID.]
    * @param String fileName [Name of the file to change the consistency state.]
    * @param String versionNumber [Version of fileName.]
    *
    * @return [void]
    */
    public void invalidation(String messageID, String originPeerID, String fileName, String versionNumber) {
        // Propogate invalidation message to peer neighbors.
        for(Peer neighbor: peer.neighbors) {
            // connect to neighboring peer
            try {
                Properties properties = new Properties();
                properties.put("org.omg.CORBA.ORBInitialPort", "1050");
                properties.put("org.omg.CORBA.ORBInitialHost", "localhost");
                // create and initialize the ORB
                ORB neighborOrb = ORB.init((String[])null, properties);

                // get the root naming context
                org.omg.CORBA.Object objRef = neighborOrb.resolve_initial_references("NameService");
                // Use NamingContextExt instead of NamingContext. This is
                // part of the Interoperable naming Service.
                NamingContextExt ncRef = NamingContextExtHelper.narrow(objRef);

                // resolve the Object Reference in Naming
                peerServerFileSharingImpl = PeerServerFileSharingHelper
                                            .narrow(ncRef.resolve_str(PEER_SERVER_NAME + neighbor.peerID + "_" + neighbor.portNumber));

                System.out.println("\nObtained a handle on neighbor " + neighbor.peerID + " server object to update file " + fileName + " to an invalid state (if the file exists in the neighboring directory): \n\n" + peerServerFileSharingImpl + "\n");

                peerServerFileSharingImpl.invalidateFile(messageID, fileName);
            }
            catch(Exception e) {
                System.out.println("ERROR : " + e) ;
                e.printStackTrace(System.out);
            }
        }
    }

    /**
    * Simulates file modification of the first file found in the local directory of owned files.
    *
    * @return [void]
    */
    public void simulateFileModification() {
        if(peer.peerID.equals("peer0")) {
            // Peer 0 has file 5 and the version of file 5 will be changed
            if(peer.localFiles.length != 0) {
                String[] versionParts = peer.localFiles[0].versionNumber.split("_");
                // Increment the version number
                String newVersionNum = String.valueOf(Integer.valueOf(versionParts[1]) + 1);

                peer.localFiles[0].versionNumber = "v_" + newVersionNum;
                System.out.println("file5.txt new version number: " + peer.localFiles[0].versionNumber);

                // Call Invalidation method to inform other neighbors of the update to the file
                invalidation(PEER_SERVER_NAME + peer.peerID + "_" + peer.portNumber, peer.peerID, peer.localFiles[0].fileName, peer.localFiles[0].versionNumber);
            }
        }
    }

    /**
     * Returns the Peer that has the requested file.
     *
     * @param String peer [description]
     *
     * @return [String]
     */
    public String peerWithFile(String peer) {
        return peer;
    }

    /**
     * Search for fileToSearch in the local peer index.
     *
     * @param String fileToSearch [File to search in the local peer index.]
     *
     * @return [boolean]
     */
    public static boolean searchForFileUsingLocalIndex(String fileToSearch) {
        System.out.println("Searching for file `" + fileToSearch + "`...");
        for(MyFileObj f: peer.localFiles) {
            //System.out.println(f.fileName);
            if(f.fileName.equals(fileToSearch)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Returns true if the peer has forwarded the associated message and false otherwise.
     *
     * @param String messageID [Message ID used to verify if the message has already been
     *                          forwarded.]
     *
     * @return [boolean]
     */
    public static boolean haveForwardedMsg(String upstreamPeer, String messageID) {
        for(MsgData md: msgData) {
            if(md.upstreamPeer.equals(upstreamPeer) && md.messageID.equals(messageID)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns true if all neighbors have received the query request and false otherwise.
     *
     * @param String neighborID [ID of the neighboring peer.]
     *
     * @return [boolean]
     */
    public static boolean haveAllNeighborsRecvd(String neighborID) {
        for(String s: neighborsRecvdMsg) {
            if(s.equals(neighborID)) {
                return true;
            }
        }

        neighborsRecvdMsg.add(neighborID);
        return false;
    }

    /**
    * Used by client peers to lookup a specified file from other peers.
    *
    * @param String messageID [Message ID that includes the peerID and port number.]
    * @param int TTL [Time to live of the query request.]
    * @param String fileName[File name to search.]
    *
    * @return [String]
    */
    public String query(String messageID, int TTL, String fileName) {
        String peerIP = "127.0.0.1";

        String[] msgParts = messageID.split("_");
        String newMsgID = msgParts[0] + "_" + peer.peerID + "_" + peer.portNumber;
        String upstreamPeer = msgParts[1];

        System.out.println("Upstream peer: " + upstreamPeer);
        System.out.println("Original messageID: " + messageID);
        System.out.println("New messageID: " + newMsgID + "\n");

        for(Peer neighbor: peer.neighbors) {
            // Get a handle on the neighbor peer server
            System.out.println("neighbor = " + neighbor.peerID + "\n");
            try {
                if(! haveForwardedMsg(upstreamPeer, messageID) && ! haveAllNeighborsRecvd(neighbor.peerID)) {
                    // Setup properties for neighboring ORB connection
                    Properties properties = new Properties();
                    properties.put("org.omg.CORBA.ORBInitialPort", "1050");
                    properties.put("org.omg.CORBA.ORBInitialHost", "localhost");
                    // create and initialize the ORB
                    ORB neighborOrb = ORB.init((String[])null, properties);

                    // get the root naming context
                    org.omg.CORBA.Object objRef = neighborOrb.resolve_initial_references("NameService");
                    // Use NamingContextExt instead of NamingContext. This is
                    // part of the Interoperable naming Service.
                    NamingContextExt ncRef = NamingContextExtHelper.narrow(objRef);

                    // resolve the Object Reference in Naming
                    peerServerFileSharingImpl = PeerServerFileSharingHelper
                                                .narrow(ncRef.resolve_str(PEER_SERVER_NAME + neighbor.peerID + "_" + neighbor.portNumber));

                    System.out.println("Obtained a handle on neighbor " + neighbor.peerID + " server object: \n\n" + peerServerFileSharingImpl + "\n");

                    // Only query neighboring peers if TTL is greater than 0 and the peer has not
                    // already fowarded the message.
                    if(TTL > 0) {
                        System.out.println("Peer: " + peer.peerID + " -> Adding to msgData associative array (" + upstreamPeer + ", " + messageID + ")...\n");
                        msgData.add(new MsgData(upstreamPeer, messageID));
                        System.out.println("Going to propogate the query with my neighbor " + neighbor.peerID + "\n");
                        peerServerFileSharingImpl.query(newMsgID, TTL-1, fileName);
                    }
                } else {
                    System.out.println("I have already fowarded this message!\n");
                }
            }
            catch(Exception e) {
                System.out.println("ERROR : " + e) ;
                e.printStackTrace(System.out);
            }
        }

        if(searchForFileUsingLocalIndex(fileName)) {
            // Use the first entry to propogate the hit query.
            return hitQuery(PEER_SERVER_NAME + peer.peerID + "_" + peer.portNumber, TTL-1, fileName, peerIP, peer.portNumber);
        }

        return hitQuery("File not found", TTL-1, fileName, peerIP, peer.portNumber);
    }

    /**
    * Used by server peer to propogate back to the original sender that the query requested
    * is successful.
    *
    * @param String messageID [Message ID that includes the peerID and port number.]
    * @param int TTL [Time to live of the hit query.]
    * @param String fileName[File name that was searched and found in the local directory of the
    *                        peer.]
    * @param int peerIP [Peer IP address]
    * @param int portNumber [Peer port number]
    *
    * @return [String]
    */
    public String hitQuery(String messageID, int TTL, String fileName, String peerIP, String portNumber) {
        if(TTL > 0) {
            // Get the first entry in the msgData associative array
            //Map.Entry<String, String> firstEntry = msgData.entrySet().iterator().next();
            //System.out.println("hitQuery -> firstEntry: " + firstEntry.getValue() + "\n");

            //if(firstEntry.getValue().equals(PEER_SERVER_NAME + "peerInteractive" + "_" + "1070") && ! messageID.equals("File not found")) {
            if(msgData.get(0).messageID.equals(PEER_SERVER_NAME + "peerInteractive" + "_" + "1070") && ! messageID.equals("File not found")) {
                System.out.println("\nWe have the original sender.");
                System.out.println("Peer with file: " + messageID + "\n");

                try {
                    File fout = new File("peer-files/interactive-peer/peersWithRequestedFile.txt");
                    FileOutputStream fos = new FileOutputStream(fout);

                    BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));

                    bw.write(messageID);
                    bw.close();
                }
                catch (Exception e) {
                    System.out.println("ERROR: " + e) ;
                    e.printStackTrace(System.out);
                }

                return messageID;
            } else if(messageID.equals("File not found")) {
                System.out.println("peer: " + peer.peerID + " did not have " + fileName);
            } else {
                // Continue to propogate the hitQuery back upstream (call hitQuery again).
                System.out.println("Need to continue to hitQuery back upstream to find original sender.\n");

                // Get a handle on the upstream peer server
                try {
                    // Setup properties for neighboring ORB connection
                    Properties properties = new Properties();
                    properties.put("org.omg.CORBA.ORBInitialPort", "1050");
                    properties.put("org.omg.CORBA.ORBInitialHost", "localhost");
                    // create and initialize the ORB
                    ORB upstreamOrb = ORB.init((String[])null, properties);

                    // get the root naming context
                    org.omg.CORBA.Object objRef = upstreamOrb.resolve_initial_references("NameService");
                    // Use NamingContextExt instead of NamingContext. This is
                    // part of the Interoperable naming Service.
                    NamingContextExt ncRef = NamingContextExtHelper.narrow(objRef);

                    // resolve the Object Reference in Naming
                    peerServerFileSharingImpl = PeerServerFileSharingHelper
                                                .narrow(ncRef.resolve_str(msgData.get(0).messageID));

                    System.out.println("Obtained a handle on the upstream peer " + msgData.get(0).messageID + " server object: \n\n" + peerServerFileSharingImpl + "\n");

                    String p = peerServerFileSharingImpl.hitQuery(PEER_SERVER_NAME + peer.peerID + "_" + peer.portNumber, TTL-1, fileName, peerIP, portNumber);

                    if(p.equals(PEER_SERVER_NAME + peer.peerID + "_" + peer.portNumber)) {
                        File fout = new File("peer-files/interactive-peer/peersWithRequestedFile.txt");
                        FileOutputStream fos = new FileOutputStream(fout);

	                    BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));

                        bw.write(p);
                        bw.close();
                    }
                }
                catch(Exception e) {
                    System.out.println("ERROR: " + e) ;
                    e.printStackTrace(System.out);
                }
            }
        }
        // File not found
        return "There was no file found...";
    }

    /**
     * Obtain downloads filename invoked by the client peer to the
     * peer server.
     *
     * @param String filename [The file requested by the client peer.]
     *
     * @return [MyFileObj]
     */
    public MyFileObj obtain(String filename, String folderPath) {
        File file = new File(folderPath + filename);

        try(BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line = null;
            String contents = "";
            while((line = br.readLine()) != null) {
                contents += line;
                contents += "\n";
            }

            String vNumber = "";
            String cState = "";
            // find fileName in local index and get version number and consistency state
            for(MyFileObj f: peer.localFiles) {
                if(f.fileName.equals(filename)) {
                    vNumber = f.versionNumber;
                    cState = f.consistencyState;
                }
            }

            System.out.println("peer: " + peer.peerID + " -> Transferring file `" + filename + "` to Interactive Peer...");

            return new MyFileObj(file.getName(), contents, file.getPath(), (int)file.length(), vNumber, cState);
        }
        catch(Exception e) {
            System.err.println("ERROR: " + e);
            e.printStackTrace(System.out);
        }

        // There was a problem retrieving the file...
        return null;
    }

    /**
     * Shutdown peer server from client peer.
     *
     * @return [void]
     */
    public void shutdown() {
        orb.shutdown(false);
    }

    /**
    * Identify and add neighbors to peer.
    *
    * @param String line [Line from config file that contains neighboring peers.]
    *
    * @return [void]
    */
    public void addNeighborsForPeer(String line) {
        int i = 0;

        String[] neighbors = line.split(" ");
        peer.neighbors = new Peer[neighbors.length-1];

        for(String neighbor: neighbors) {
            if(! neighbor.contains(peer.peerID)) {
                peer.neighbors[i] = new Peer();
                peer.neighbors[i].peerID = "peer" + neighbor;
                peer.neighbors[i].portNumber = "105" + neighbor;
                //System.out.println(peer.neighbors[i].peerID);
                i++;
            }
        }
    }

    /**
    * Initialize the network configuration using networkConfigFile (set up neighbors for
    * each peer), set the peer ID and peer port number.
    *
    * @param String networkConfigFile [Configuration file used to setup the network topology
    *                                  for each peer.]
    * @param String pID [Peer id used for port number as well, beginning at 1050.]
    *
    * @return [void]
    */
    public void initNetworkConfigAndSetPeerIDAndPortNumber(String pID, String networkConfigFile) {
        BufferedReader br = null;
        String readLine = "";

        peer.peerID = "peer" + pID;
        peer.portNumber = "105" + pID;
        //System.out.println(peer.peerID);

        try {
            br = new BufferedReader(new FileReader(networkConfigFile));

            while((readLine = br.readLine()) != null) {
                if(readLine.contains(peer.peerID)) {
                    addNeighborsForPeer(readLine);
                }
            }
        } catch(FileNotFoundException e) {
            System.err.println("Error: " + e.getMessage());
            e.printStackTrace();
        } catch(IOException e){
            System.err.println("Error: " + e.getMessage());
            e.printStackTrace();
        } finally {
            try {
                if(br != null) {
                    br.close();
                }
            } catch(IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Sets a random file size for file within the peerFolderPath.
     * File sizes range from 1 to 15 KB.
     *
     * @param String peerFolderPath [Path to the folder that holds the files for the peer
     *                                within the project directory.]
     * @param File file [File that will be given a random file size.]
     *
     * @return [void]
     */
    public void setRandomFileSize(String peerFolderPath, File file) {
        try {
            File f = new File(peerFolderPath + file.getName());
            RandomAccessFile raf = new RandomAccessFile(f, "rw");
            // Set file random size between 1 and 15 KB
            Random rn = new Random();
            int randFileSize = rn.nextInt(15 - 1 + 1) + 1;

            raf.setLength(1024 * randFileSize);
        }
        catch(Exception e) {
            System.out.println("ERROR : " + e) ;
            e.printStackTrace(System.out);
        }
    }

    /**
    * Peer will have it files registered from the corresponding directory.
    *
    * @return [void]
    */
    public void peerServerIndexLocalFiles() {
        File peerFolderPath = new File("peer-files/" + peer.peerID);
        File[] peerFiles = peerFolderPath.listFiles();

        List<MyFileObj> files = new ArrayList<MyFileObj>();

        for(File f: peerFiles) {
            if(f.isFile()) {
                //setRandomFileSize(peerFolderPath, f);

                try(BufferedReader br = new BufferedReader(new FileReader("peer-files/"+ peer.peerID + "/" + f.getName()))) {
                    String line = null;
                    String contents = "";
                    while((line = br.readLine()) != null) {
                        contents += line;
                        contents += "\n";
                    }

                    MyFileObj myfile = new MyFileObj(f.getName(), contents, "peer-files/" + peer.peerID + "/", (int)f.length(), "v_1", "valid");
                    // System.out.println(myfile.fileName);
                    // System.out.println(myfile.fileData);
                    files.add(myfile);
                }
                catch(Exception e) {
                    System.err.println("ERROR: " + e);
                    e.printStackTrace(System.out);
                }
            }
        }

        peer.localFiles = files.toArray(new MyFileObj[0]);
    }
}

public class IndexPeer
{
    public static final String PEER_SERVER_NAME = "PeerServer_peer";

    static PeerServerFileSharing peerServerFileSharingImpl;

    static final int PEER_SERVER_THREAD_POOL_SIZE = 5;

    // Create thread pool to handle multiple requests concurrently
    static ExecutorService peerThreadPool = Executors.newFixedThreadPool(PEER_SERVER_THREAD_POOL_SIZE);

    /**
     * Initialize the peer server so that it can receive invocations from other client
     * peers.
     *
     * @param String args[]
     *
     * @return [void]
     */
    public static void initPeerServer(String args[]) {
        // Thread pool will handle multiple requests from other peer clients
        peerThreadPool.submit(() -> {
            try {
                // create and initialize the peer server ORB
                ORB peerServerORB = ORB.init(args, null);

                // get reference to rootpoa & activate the POAManager
                POA rootpoa = POAHelper.narrow(peerServerORB.resolve_initial_references("RootPOA"));
                rootpoa.the_POAManager().activate();

                // create servant and register it with the peer server ORB
                PeerServerFileSharingImpl peerServerImpl = new PeerServerFileSharingImpl();
                peerServerImpl.setORB(peerServerORB);

                // Initialize the network topology
                peerServerImpl.initNetworkConfigAndSetPeerIDAndPortNumber(args[0], args[1]);

                // Register local files for the peer
                peerServerImpl.peerServerIndexLocalFiles();

                // Simulate file modification
                peerServerImpl.simulateFileModification();

                // get object reference from the servant
                org.omg.CORBA.Object ref = rootpoa.servant_to_reference(peerServerImpl);
                PeerServerFileSharing href = PeerServerFileSharingHelper.narrow(ref);

                // get the root naming context
                // NameService invokes the name service
                org.omg.CORBA.Object objRef =
                    peerServerORB.resolve_initial_references("NameService");
                // Use NamingContextExt which is part of the Interoperable
                // Naming Service (INS) specification.
                NamingContextExt ncRef = NamingContextExtHelper.narrow(objRef);

                // bind the Object Reference in Naming
                NameComponent path[] = ncRef.to_name(PEER_SERVER_NAME + args[0] + "_105" + args[0]);
                ncRef.rebind(path, href);

                System.out.println("\nPeer server " + args[0] + " ready and waiting...\n");

                // wait for invocations from other peer clients
                peerServerORB.run();
            }
            catch(Exception e) {
                System.err.println("ERROR: " + e);
                e.printStackTrace(System.out);
            }
        });
    }

    /**
     * Main method of the index peer. Starts the peer server, configures the network topology
     * and waits for queries from other peers.
     *
     * @param String args[] [Two arguments are passed to start the peer. First is the peer
     *                       id: 1 or 2 or 3... etc. and the second is the config file to
     *                       determine the network configuration (configStarTopology.txt or
     *                       config2DMeshTopology.txt).]
     * @return [void]
     */
    public static void main(String args[]) {
        if(args.length == 0) {
            System.out.print("ERROR -> usage: java IndexPeer 'peer ID' 'networkConfig file'\n");
            System.out.println("Where 'peer ID' is 1 or 2 or 3\nPlease try again.");
            System.exit(1);
        }

        // Initialize the peer as a server
        initPeerServer(args);
    }
}
