package DistFileSharingApp;


/**
* DistFileSharingApp/MyFileObjHelper.java .
* Generated by the IDL-to-Java compiler (portable), version "3.2"
* from DistFileSharingApp.idl
* Monday, May 1, 2017 8:07:20 PM CDT
*/

abstract public class MyFileObjHelper
{
  private static String  _id = "IDL:DistFileSharingApp/MyFileObj:1.0";

  public static void insert (org.omg.CORBA.Any a, DistFileSharingApp.MyFileObj that)
  {
    org.omg.CORBA.portable.OutputStream out = a.create_output_stream ();
    a.type (type ());
    write (out, that);
    a.read_value (out.create_input_stream (), type ());
  }

  public static DistFileSharingApp.MyFileObj extract (org.omg.CORBA.Any a)
  {
    return read (a.create_input_stream ());
  }

  private static org.omg.CORBA.TypeCode __typeCode = null;
  private static boolean __active = false;
  synchronized public static org.omg.CORBA.TypeCode type ()
  {
    if (__typeCode == null)
    {
      synchronized (org.omg.CORBA.TypeCode.class)
      {
        if (__typeCode == null)
        {
          if (__active)
          {
            return org.omg.CORBA.ORB.init().create_recursive_tc ( _id );
          }
          __active = true;
          org.omg.CORBA.StructMember[] _members0 = new org.omg.CORBA.StructMember [6];
          org.omg.CORBA.TypeCode _tcOf_members0 = null;
          _tcOf_members0 = org.omg.CORBA.ORB.init ().create_string_tc (0);
          _members0[0] = new org.omg.CORBA.StructMember (
            "fileName",
            _tcOf_members0,
            null);
          _tcOf_members0 = org.omg.CORBA.ORB.init ().create_string_tc (0);
          _members0[1] = new org.omg.CORBA.StructMember (
            "fileData",
            _tcOf_members0,
            null);
          _tcOf_members0 = org.omg.CORBA.ORB.init ().create_string_tc (0);
          _members0[2] = new org.omg.CORBA.StructMember (
            "folderPath",
            _tcOf_members0,
            null);
          _tcOf_members0 = org.omg.CORBA.ORB.init ().get_primitive_tc (org.omg.CORBA.TCKind.tk_long);
          _members0[3] = new org.omg.CORBA.StructMember (
            "fileSize",
            _tcOf_members0,
            null);
          _tcOf_members0 = org.omg.CORBA.ORB.init ().create_string_tc (0);
          _members0[4] = new org.omg.CORBA.StructMember (
            "versionNumber",
            _tcOf_members0,
            null);
          _tcOf_members0 = org.omg.CORBA.ORB.init ().create_string_tc (0);
          _members0[5] = new org.omg.CORBA.StructMember (
            "consistencyState",
            _tcOf_members0,
            null);
          __typeCode = org.omg.CORBA.ORB.init ().create_struct_tc (DistFileSharingApp.MyFileObjHelper.id (), "MyFileObj", _members0);
          __active = false;
        }
      }
    }
    return __typeCode;
  }

  public static String id ()
  {
    return _id;
  }

  public static DistFileSharingApp.MyFileObj read (org.omg.CORBA.portable.InputStream istream)
  {
    DistFileSharingApp.MyFileObj value = new DistFileSharingApp.MyFileObj ();
    value.fileName = istream.read_string ();
    value.fileData = istream.read_string ();
    value.folderPath = istream.read_string ();
    value.fileSize = istream.read_long ();
    value.versionNumber = istream.read_string ();
    value.consistencyState = istream.read_string ();
    return value;
  }

  public static void write (org.omg.CORBA.portable.OutputStream ostream, DistFileSharingApp.MyFileObj value)
  {
    ostream.write_string (value.fileName);
    ostream.write_string (value.fileData);
    ostream.write_string (value.folderPath);
    ostream.write_long (value.fileSize);
    ostream.write_string (value.versionNumber);
    ostream.write_string (value.consistencyState);
  }

}
