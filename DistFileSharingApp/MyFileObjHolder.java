package DistFileSharingApp;

/**
* DistFileSharingApp/MyFileObjHolder.java .
* Generated by the IDL-to-Java compiler (portable), version "3.2"
* from DistFileSharingApp.idl
* Monday, May 1, 2017 8:07:20 PM CDT
*/

public final class MyFileObjHolder implements org.omg.CORBA.portable.Streamable
{
  public DistFileSharingApp.MyFileObj value = null;

  public MyFileObjHolder ()
  {
  }

  public MyFileObjHolder (DistFileSharingApp.MyFileObj initialValue)
  {
    value = initialValue;
  }

  public void _read (org.omg.CORBA.portable.InputStream i)
  {
    value = DistFileSharingApp.MyFileObjHelper.read (i);
  }

  public void _write (org.omg.CORBA.portable.OutputStream o)
  {
    DistFileSharingApp.MyFileObjHelper.write (o, value);
  }

  public org.omg.CORBA.TypeCode _type ()
  {
    return DistFileSharingApp.MyFileObjHelper.type ();
  }

}
