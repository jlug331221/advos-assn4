import DistFileSharingApp.*;
import org.omg.CosNaming.*;
import org.omg.CosNaming.NamingContextPackage.*;
import org.omg.CORBA.*;
import org.omg.PortableServer.*;
import org.omg.PortableServer.POA;

import java.net.Socket;

import java.util.*;
import java.io.*;

public class InteractivePeer
{
    static PeerServerFileSharing peerServerFileSharingImpl;

    public static final String PEER_SERVER_NAME = "PeerServer_peer";

    static final String INTERACTIVE_PEER_DOWNLOAD_FOLDER_PATH = "peer-files/interactive-peer/download/";

    static String peerWithFile = "";

    static Scanner kbd1 = new Scanner(System.in);
    static Scanner kbd2 = new Scanner(System.in);

    /**
     * Add file to interactive peer download directory.
     *
     * @param MyFileObj myFileObj [File to add to the interactive peer download directory.]
     * @return [void]
     */
    public static void addFileToInteractivePeerDownloadFolder(MyFileObj myFileObj) {
        BufferedWriter bw = null;
        FileWriter fw = null;

        File f = new File(INTERACTIVE_PEER_DOWNLOAD_FOLDER_PATH, myFileObj.fileName);

        try {
            if (! f.exists()) {
                f.createNewFile();

                // Write contents from downloaded file to new file in the interactive peer folder
                fw = new FileWriter(f);
    	        bw = new BufferedWriter(fw);
    	        bw.write(myFileObj.fileData);

                // Set the correct file size
                RandomAccessFile raf = new RandomAccessFile(f, "rw");
                raf.setLength(myFileObj.fileSize);

                System.out.println("\nFile `" + myFileObj.fileName + "` successfully downloaded.");
                System.out.println("File version: " + myFileObj.versionNumber);
                System.out.println("File state: " + myFileObj.consistencyState + "\n");
            }
        }
        catch(Exception e) {
            System.out.println("ERROR : " + e) ;
            e.printStackTrace(System.out);
        }
        finally {
			try {
				if (bw != null)
					bw.close();

				if (fw != null)
					fw.close();
			}
            catch (IOException ex) {
				ex.printStackTrace();
			}
        }
    }

    /**
     * Get user input as a file name to search. Use the file name to search for all clients
     * that have said file name and download it to client peer directory.
     *
     * @param String[] args [Needed for the initialization of the ORB]
     *
     * @return [void]
     */
    public static void getUserInputForFiletoDownload(String[] args) {
        String fileToSearch = "";
        String correctFileNameAnswer = "";

        System.out.println("Note: The only file extensions available for searching are '.txt' and '.dat'");
        System.out.println("\nPress 'Q' to quit.");
        System.out.print("Search for a file: ");
        fileToSearch = kbd1.nextLine();

        while(! fileToSearch.equalsIgnoreCase("Q")) {
            System.out.print("\nIt looks like you want to search for `" + fileToSearch + "`. Is that correct? 'Y' or 'N': ");
            correctFileNameAnswer = kbd1.nextLine();

            if(correctFileNameAnswer.equalsIgnoreCase("Y")) {
                // Perform search
                try {
                    // create and initialize the ORB
                    ORB orb = ORB.init(args, null);

                    // get the root naming context
                    org.omg.CORBA.Object objRef = orb.resolve_initial_references("NameService");
                    // Use NamingContextExt instead of NamingContext. This is
                    // part of the Interoperable naming Service.
                    NamingContextExt ncRef = NamingContextExtHelper.narrow(objRef);

                    // resolve the Object Reference in Naming
                    peerServerFileSharingImpl = PeerServerFileSharingHelper
                                                .narrow(ncRef.resolve_str(PEER_SERVER_NAME + "0_1050"));

                    System.out.println("Obtained a handle on peer server 0 object: \n\n" + peerServerFileSharingImpl + "\n");

                    // Send a query for file1.txt with a TTL value of 10
                    peerServerFileSharingImpl.query(PEER_SERVER_NAME + "Interactive_1070", 10, fileToSearch);

                    // Get peer with file from .txt in interactive-peer directory
                    String peerWithFile = "";
                    //String lineInput;
                    InputStream fis = new FileInputStream("peer-files/interactive-peer/peersWithRequestedFile.txt");
                    InputStreamReader isr = new InputStreamReader(fis);
                    BufferedReader br = new BufferedReader(isr);

                    // Use first peer to download file.
                    peerWithFile = br.readLine();
                    /*while((lineInput = br.readLine()) != null) {
                        peerWithFile = lineInput;
                    }*/

                    if(peerWithFile != null) {
                        String[] parts = peerWithFile.split("_");
                        // connect to peer and download file
                        peerServerFileSharingImpl = PeerServerFileSharingHelper
                                                .narrow(ncRef.resolve_str(peerWithFile));

                        System.out.println("Obtained a handle on peer server object with the requested file to download: \n\n" + peerServerFileSharingImpl + "\n");

                        MyFileObj desiredFile = peerServerFileSharingImpl.obtain(fileToSearch, "peer-files/" + parts[1] + "/");

                        addFileToInteractivePeerDownloadFolder(desiredFile);
                    } else {
                        System.out.println("No peers have the requested file... Sorry\n");
                    }
                }
                catch(Exception e) {
                    System.out.println("ERROR : " + e) ;
                    e.printStackTrace(System.out);
                }
            }

            System.out.println("\nPress 'Q' to quit.");
            System.out.print("Search for a file: ");
            fileToSearch = kbd1.nextLine();
        }

        System.out.println("\nThank you for using this Distributed File Sharing System! Come back soon for all of your illegal downloads.\n");
    }

    /**
     * Interactive peer main method.
     *
     * @param String args[]
     *
     * @return [void]
     */
    public static void main(String args[]) {
        // Prompt user to search for a file to download
        getUserInputForFiletoDownload(args);
    }
}
